import Vue from 'vue'
import App from './App'
import router from './router'
import Icon from './components/Icon.vue'

import 'materialize-css/dist/css/materialize.min.css'
import 'jquery'
import 'materialize-css/dist/js/materialize.min.js'

Vue.config.productionTip = false
Vue.component('icon', Icon)

window.store = new Vue({
  data: () => ({
    registrationForm: {
      firstName: null,
      lastName: null,
      sex: null
    },
    registerDone: false,
    testUndone: true,
    test: [
      {
        text: 'Меланома в анамнезе',
        score: 1
      },
      {
        text: 'Увеличение числа невусов',
        score: 1,
        info: {
          header: 'Увеличение числа невусов',
          text: 'Увеличение числа невусов после пубертатного периода более 50'
        }
      },
      {
        text: 'Наличие в анамнезе длительного периода солнечного воздействия',
        score: 1
      },
      {
        text:
          'Отягощенный семейный анамнез (меланома у одного родственника 1-й степени родства)',
        score: 1
      },
      {
        text: 'Рак кожи в семейном анамнезе',
        score: 1
      },
      {
        text: 'I или II тип фоточувствительности кожи по Фицпатрику',
        score: 1,
        img: [
          require('@/assets/img/I.jpg'),
          require('@/assets/img/II.jpg'),
          require('@/assets/img/3.jpg'),
          require('@/assets/img/4.jpg'),
          require('@/assets/img/5.jpg')
        ]
      },
      {
        text: 'Множественны (11-100) невусы',
        score: 1
      },
      {
        text: 'Веснушки',
        score: 1
      },
      {
        text: 'Естественный рыжий или светлый цвет волос',
        score: 1
      },
      {
        text: '1-3 атипичных невуса',
        score: 1,
        info: {
          header: 'Атипичные невусы',
          text:
            'Атипичные невусы (5-6мм в диаметре, ассимпетричная форма, неправильные границы, несколько цветов, похожи на "яичницу глазунью")',
          img: [
            require('@/assets/img/modals/atype/атипичный невус1.jpg'),
            require('@/assets/img/modals/atype/атипичный невус2.jpg'),
            require('@/assets/img/modals/atype/атипичный невус3.jpg')
          ]
        }
      },
      {
        text: 'Актиническое повреждение кожи',
        score: 1,
        info: {
          header: 'Актиническое повреждение',
          text:
            'Актиническое повреждение кожи (пигментные пятна на открытых участках кожного покрова, глубокие морщины)',
          img: [
            require('@/assets/img/modals/act/актинические изменения.jpg'),
            require('@/assets/img/modals/act/актинич повреждения2.jpg'),
            require('@/assets/img/modals/act/актнич 3.jpg')
          ]
        }
      },
      {
        text: 'Светлые глаза',
        score: 1
      },
      {
        text: 'Светлая кожа',
        score: 1
      },
      {
        text: 'Наличие рака кожи в анамнезе',
        score: 20
      },
      {
        text:
          'Наличие в анамнезе чрезмерного воздействия солнечного излучения и рака кожи в анамнезе',
        score: 20
      },
      {
        text: 'Иммуносупрессия по причине пересадки органов',
        score: 20
      },
      {
        text:
          'Более 250 процедур с использованием псоралена и ультрафиолетового излучения (ПУВА-терапия) для лечения псориаза',
        score: 20
      },
      {
        text: 'Лучевая терапия в детском возрасте',
        score: 20
      },
      {
        text:
          'Наличие в анамнезе множественных меланом или рака поджелудочной железы',
        score: 20
      },
      {
        text:
          'Мутация CDKN2Aу членов семьи (меланома, множественные атипичные невусы)',
        score: 20
      },
      {
        text:
          'Два или более случаев меланомы у родственников 1-й степени родства',
        score: 20
      },
      {
        text: 'Более 5 атипичных невусов',
        score: 20
      },
      {
        text: 'Более 100 невусов больше 2 мм в диаметре',
        score: 20
      },
      {
        text:
          'Большой врожденный невус больше 20 см или больше 5% поверхности тела',
        score: 20
      }
    ]
  }),
  computed: {
    score() {
      let sum = 0

      for (let o of this.test) {
        if (o.value) {
          sum += o.score
        }
      }

      return sum
    },
    riskGroup() {
      if (this.score > 19) {
        return 2
      } else if (this.score > 0) {
        return 1
      } else {
        return 0
      }
    }
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

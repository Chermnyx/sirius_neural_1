import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
os.environ['CUDA_VISIBLE_DEVICES'] = '3'

import keras
from keras.models import Model
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense, Conv2D, MaxPooling2D, Activation, ZeroPadding2D, Input
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras import backend as K
K.set_image_data_format('channels_last')

img_rows, img_cols = 224, 224
if K.image_data_format() == "channels_first":
    input_shape = (3, img_rows, img_cols)
else:
    input_shape = (img_rows, img_cols, 3)

from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

train_data_dir = './512/train'
validation_data_dir = './512/val'
nb_train_samples = 3248
nb_validation_samples = 800
nb_classes = 2
epochs = 5
batch_size = 8

base_model = applications.vgg19.VGG19(include_top=False, weights='imagenet',input_shape=input_shape)

for l in base_model.layers:
    l.trainable = False

x = base_model.output
#x = Conv2D(512, (2, 2), activation='relu', name='block6_conv1')(x)
#x = MaxPooling2D((2, 2), name='block6_pool')(x)
#x = Conv2D(1024, (2,2), activation='relu', name='block7_conv1')(x)
#x = MaxPooling2D((2,2), name='block7_pool')(x)
x = Flatten()(x)
x = Dense(128, activation='relu')(x)
x = Dropout(0.5)(x)
#x = Dense(32, activation='relu')(x)
#x = Dropout(0.5)(x)
prediction = Dense(1, activation='sigmoid')(x)

model = Model(inputs=base_model.input, outputs=prediction)

print(model.summary())
#exit()

model.compile(
    loss='binary_crossentropy',
    optimizer=optimizers.RMSprop(lr=0.0001),
    metrics=['accuracy'])

checkpoint = ModelCheckpoint(
    'bests_weight_1.h5', monitor="val_acc", verbose=1, save_best_only=True)

reduce_lr = ReduceLROnPlateau(
    monitor='val_acc', factor=0.2, patience=2, min_lr=0.000001, verbose=1)

stop = EarlyStopping(monitor='val_acc', patience=5, verbose=1)

callbacks_list = [checkpoint, stop]

train_datagen = ImageDataGenerator(
#    rescale=1. / 255,
#    rotation_range=360,
#    width_shift_range=0.2,
#    height_shift_range=0.2,
#    shear_range=0.2,
#    zoom_range=0.2,
#    horizontal_flip=True,
#    vertical_flip=True
    )

test_datagen = ImageDataGenerator()

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_rows, img_cols),
    batch_size=batch_size,
#    save_to_dir='./t_aug224-1',
    class_mode='binary'
    )

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_rows, img_cols),
    batch_size=batch_size,
#    save_to_dir='./v_aug224-1',
    class_mode='binary'
    )

# fine-tune the model
model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size,
    callbacks=callbacks_list)

scores = model.evaluate_generator(validation_generator, )
print("%s: %.2f%% with" % (model.metrics_names[1], scores[1] * 100))

import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

import numpy as np

import keras
from keras.models import Model
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.layers import Concatenate, BatchNormalization, SeparableConv2D, Input, Dropout, Flatten, Dense, Conv2D, MaxPooling2D, Activation
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras import backend as K
K.set_image_data_format('channels_last')

img_rows, img_cols = 256, 256
if K.image_data_format() == "channels_first":
    input_shape = (3, img_rows, img_cols)
else:
    input_shape = (img_rows, img_cols, 3)

from tensorflow.python.client import device_lib

train_data_dir = './512/train'
validation_data_dir = './512/val'
nb_train_samples = 3248
nb_validation_samples = 800
nb_classes = 2
epochs = 100
batch_size = 32
dropout = 0

input = Input(shape=input_shape)
x = SeparableConv2D(64, (7, 7), strides=(2, 2), padding='same')(input)
x = BatchNormalization()(x)
x = Activation('relu')(x)
if dropout > 0:
    x = Dropout(dropout)(x)
x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

x = SeparableConv2D(64, (1, 1), padding='same')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = SeparableConv2D(192, (3, 3), padding='same')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

a = SeparableConv2D(128, (1, 1), padding='same')(x)
a = BatchNormalization()(a)
a = Activation('relu')(a)

b = SeparableConv2D(128, (3, 3), padding='same')(x)
b = BatchNormalization()(b)
b = Activation('relu')(b)

c = SeparableConv2D(128, (5, 5), padding='same')(x)
c = BatchNormalization()(c)
c = Activation('relu')(c)

x = keras.layers.concatenate([a,b,c])
x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

a = SeparableConv2D(128, (1, 1), padding='same')(x)
a = BatchNormalization()(a)
a = Activation('relu')(a)
a = SeparableConv2D(128, (1, 1), padding='same')(x)
a = BatchNormalization()(a)
a = Activation('relu')(a)

b = SeparableConv2D(128, (3, 3), padding='same')(x)
b = BatchNormalization()(b)
b = Activation('relu')(b)
b = SeparableConv2D(128, (3, 3), padding='same')(x)
b = BatchNormalization()(b)
b = Activation('relu')(b)

c = SeparableConv2D(128, (5, 5), padding='same')(x)
c = BatchNormalization()(c)
c = Activation('relu')(c)
c = SeparableConv2D(128, (5, 5), padding='same')(x)
c = BatchNormalization()(c)
c = Activation('relu')(c)

x = keras.layers.concatenate([a,b,c])
x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

a = SeparableConv2D(128, (1, 1), padding='same')(x)
a = BatchNormalization()(a)
a = Activation('relu')(a)

b = SeparableConv2D(128, (3, 3), padding='same')(x)
b = BatchNormalization()(b)
b = Activation('relu')(b)

c = SeparableConv2D(128, (5, 5), padding='same')(x)
c = BatchNormalization()(c)
c = Activation('relu')(c)

x = keras.layers.concatenate([a,b,c])
x = MaxPooling2D(pool_size=(7, 7))(x)

x = Flatten()(x)
#x = Dense(1000, activation='relu')(x)
output = Dense(1, activation='sigmoid')(x)

top_model = Model(input, output)

print(top_model.summary())

top_model.compile(
    loss='binary_crossentropy',
    optimizer=optimizers.Adam(lr=0.00005),
    metrics=['accuracy'])

checkpoint = ModelCheckpoint(
    'bests_weight.h5', monitor="val_acc", verbose=1, save_best_only=True)

reduce_lr = ReduceLROnPlateau(
    monitor='val_acc', factor=0.5, patience=10, min_lr=0.0000001, verbose=1)

stop = EarlyStopping(monitor='val_acc', patience=20, verbose=1)

callbacks_list = [checkpoint, reduce_lr, stop]

# prepare data augmentation configuration
train_datagen = ImageDataGenerator(
#    rescale=1. / 255,
    rotation_range=360,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    vertical_flip=True
     )

test_datagen = ImageDataGenerator()

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_rows, img_cols),
    batch_size=batch_size,
    class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_rows, img_cols),
    batch_size=batch_size,
    class_mode='binary')

# fine-tune the model
top_model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size,
    callbacks=callbacks_list)

scores = top_model.evaluate_generator(validation_generator, )
print("%s: %.2f%% with" % (top_model.metrics_names[1], scores[1] * 100))
